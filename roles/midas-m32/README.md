# bash

Manage and configure the M32 Edit utility for Midas M32R sound mixers.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

There are no available variables in this role.
