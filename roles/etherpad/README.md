# etherpad

Install an etherpad-lite instance.

## Tasks

The tasks are divided this way:

* `tasks/nginx.yml`: Manage nginx and TLS certificates.

* `tasks/etherpad.yml`: Manage etherpad installation and configuration

## Available variables

Main variables are:

* `etherpad.server_name`: The FQDN of the etherpad instance.

* `etherpad.title`: The title of the etherpad instance.

* `etherpad.favicon`: Optional. URL to the favicon of the etherpad instance.

* `etherpad.salsa_client_id`: Client ID for OpenID Connect to Salsa.
                              Optional, public when unset.

* `etherpad.salsa_client_secret`: Client Secret for OpenID Connect to Salsa.
                                  Optional, public when unset.

* `etherpad.permit_author_name_change`: Optional boolean. Allow authenticated users to change their names.

* `etherpad.version`: Version of etherpad-lite to install.

* `letsencrypt_well_known_dir`: Directory where to store the `/.well-known/` data
                                for the Let's Encrypt `http-01` challenge.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
