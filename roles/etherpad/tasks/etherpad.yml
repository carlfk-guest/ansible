---
- name: Install etherpad-related packages
  ansible.builtin.apt:
    name:
      - npm
      - nginx
      - postgresql
      - python3-psycopg2
      - systemd

- name: Create etherpad Postgres user
  community.postgresql.postgresql_user:
    name: etherpad
  become: true
  become_user: postgres
  when: not skip_unit_test

- name: Create etherpad Postgres DB
  community.postgresql.postgresql_db:
    name: etherpad
    owner: etherpad
  become: true
  become_user: postgres
  when: not skip_unit_test

- name: Create gitconfig to workaround npm problems
  # npm 6 tries to hit GitHub on git:// which hangs forever
  ansible.builtin.copy:
    src: files/gitconfig
    dest: /root/.gitconfig

- name: Create etherpad directory
  ansible.builtin.file:
    state: directory
    dest: /srv/etherpad

- name: Download etherpad
  ansible.builtin.get_url:
    url: "https://github.com/ether/etherpad-lite/archive/\
          {{ etherpad.version }}.tar.gz"
    dest: /srv/etherpad-{{ etherpad.version }}.tar.gz
  notify:
    - Extract etherpad
    - Install etherpad deps

- name: Flush handlers
  ansible.builtin.meta: flush_handlers

- name: Create etherpad user
  ansible.builtin.user:
    name: etherpad
    comment: etherpad-lite user
    shell: /bin/bash
    home: /srv/etherpad

- name: Create etherpad writeable directories
  ansible.builtin.file:
    state: directory
    dest: "{{ item }}"
    owner: etherpad
    group: etherpad
  with_items:
    - /var/log/etherpad
    - /srv/etherpad/var
    - /srv/etherpad/.npm

- name: Install etherpad settings
  ansible.builtin.template:
    src: templates/settings.json.j2
    dest: /srv/etherpad/settings.json
  notify: Restart etherpad

- name: Generate etherpad keys
  ansible.builtin.shell:
    cmd: "dd if=/dev/random count=1 \
          | sha512sum > /srv/etherpad/{{ item }}"  # noqa: risky-shell-pipe
    creates: /srv/etherpad/{{ item }}
  with_items:
    - APIKEY.txt
    - SESSIONKEY.txt

- name: Install etherpad package.json
  ansible.builtin.copy:
    src: package.json
    dest: /srv/etherpad/package.json
  notify:
    - Install etherpad plugins
    - Restart etherpad

- name: Install etherpad systemd unit
  ansible.builtin.copy:
    src: files/etherpad.service
    dest: /etc/systemd/system/etherpad.service
  notify:
    - Reload-systemd
    - Restart etherpad

- name: Enable etherpad systemd units
  ansible.builtin.systemd:
    service: etherpad.service
    enabled: true
  when: not skip_unit_test

- name: Push etherpad-exporter script
  ansible.builtin.copy:
    src: files/etherpad-exporter.py
    dest: /usr/local/bin/etherpad-exporter
    mode: "0755"
