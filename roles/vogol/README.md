# vogol

Manage and configure the web control interface for voctomix

## Tasks

The tasks are divided this way:

* `tasks/nginx.yml`: Manage nginx and TLS certificates.

* `tasks/vogol.yml`: Manage package installation and the vogol files.

## Available variables

Main variables are:

* `room_name`: Name of the room where you are recording. Displayed to the user.

* `user_name`:                    Main user username.

* `vogol.presets`: Optional. Dictionary of presets, keyed by preset ID.

* `vogol.presets.[].name`: String. The preset's name, displayed in UI.

* `vogol.presets.[].video_a`: String. The preset's A video source.

* `vogol.presets.[].video_b`: Optional String. The preset's B video source.

* `vogol.presets.[].composite_mode`: String. The preset's composite mode.

* `vogol.presets.[].audio_solo`: List of strings. The audio sources to solo for the preset.

* `vogol.recordings`: Directory containing videos to play back.

* `vogol.server_name`: The FQDN of the vogol server.

* `vogol.gitlab_client_id`: Client ID for OpenID Connect to Gitlab.
                              Optional, public when unset.

* `vogol.gitlab_client_secret`: Client Secret for OpenID Connect to Gitlab.
                                  Optional, public when unset.

* `vogol.gitlab_name`: String. Name of the Gitlab server, to display to the user.

* `vogol.gitlab_url`: URL of the Gitlab server to use.
                        Optional, public when unset.

* `vogol.gitlab_group`: Group on gitlab, required for access.
                          Optional, any when unset.

* `vogol.video_only_sources`: Optional. List of strings. Names of sources that don't have audio.

* `letsencrypt_well_known_dir`: Directory where to store the `/.well-known/` data
                                for the Let's Encrypt `http-01` challenge.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
