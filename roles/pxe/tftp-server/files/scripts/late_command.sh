#!/bin/sh
# Ansible Managed

set -efux

# This script (late_command.sh) sets up ansible
# and a systemd script to run it on next boot.
# It should be ran at the end of the basic installation of a machine.
# It takes one parameter, the base url that was passed to d-i.
# This should be a bare hostname or IP of the PXE server.

server=$1

if [ -n "${lc2_url:-}" ]; then
    # Override this late_command.sh and get/run a custom one.
    # exit 1 will trigger the installer to error and wait for user input, then finish.
    # Note: exec will not return here, so the rest of this script does not run.
    # so do this:
    # unset lc2_url; /tmp/late_command.sh $1
    cd /tmp
    wget $lc2_url -O lc2.sh
    chmod u+x lc2.sh
    exec ./lc2.sh $server
fi

wget http://$server/scripts/late_command.cfg -O /tmp/late_command.cfg
. /tmp/late_command.cfg

apt-get install -y git eatmydata kbd
# Ansible >= 2.4
suite=$(lsb_release -cs)
case $suite in
    xenial|artful|bionic)
        apt-get install -y software-properties-common
        apt-add-repository --yes --update ppa:ansible/ansible
        apt-get install -y ansible
        ;;
    *)
        apt-get install -y ansible
        ;;
esac

# clone our ansible repository(s)

git clone $playbook_repo /root/playbook-repo
(cd /root/playbook-repo; git checkout $playbook_branch)
INVENTORY=/root/playbook-repo/inventory/hosts
PLAYBOOKS=/root/playbook-repo/site.yml

git clone $inventory_repo /root/inventory-repo -b $inventory_branch
INVENTORY=/root/inventory-repo/$inventory_repo_dir/hosts
if [ -e /root/inventory-repo/$inventory_repo_dir/site.yml ]; then
    PLAYBOOKS="$PLAYBOOKS /root/inventory-repo/$inventory_repo_dir/site.yml"
fi

if [ -n "$vault_pw" ]; then
    echo "$vault_pw" | base64 -d > /root/.ansible-vault
    chmod 600 /root/.ansible-vault
fi

# download a script to run ansible on the local box.
script=/usr/local/sbin/ansible-up
wget http://$server/scripts/ansible-up -O $script
chmod +x $script

wget http://$server/scripts/ansible-firstboot.service \
    -O /etc/systemd/system/ansible-firstboot.service
wget http://$server/scripts/ansible-firstboot.sh \
    -O /usr/local/sbin/ansible-firstboot
chmod +x /usr/local/sbin/ansible-firstboot

systemctl enable ansible-firstboot.service
