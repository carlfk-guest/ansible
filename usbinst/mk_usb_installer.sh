#!/bin/bash

set -eux

# build a usb installer: debian, preseed, ansible
# expects networking in order to get:
#   preseed, deb repos, late_command, ansible, blobs

# $1 - /dev/sdx1 for usb stick to clobber
# $2 - optional filename of parameters

dev=$1
label=${dev#/dev/}
if [ ! -b "${dev}" ]; then
      echo "error: ${dev} is not a block device."
      exit
fi

if findmnt --source ${dev}; then
      echo "error: ${dev} has mounted fs's."
      exit
fi

# default parameters that can be over ridden by $2 passed config file
hostname=base
## domain=

# where to get what installer binaries:
distro=debian
suite=bookworm
arch=amd64
archive_mirror=http://deb.debian.org
cdimage_mirror=https://cdimage.debian.org

# where to get the preseed file for the installer
# default: video-setup.debian.net
# d-i|scripts redirects to https://salsa.debian.org/debconf-video-team/ansible/raw/main/roles/pxe/tftp-server/files/
url=video-setup.debian.net

# don't ask about firmware  (may be needed for network device)
load_firmware=false

# Anything else you want to append to the kernel.
more_appends=

# end of default parameters.

# if passed, load .conf file
if [ $# -ge 2 ]; then
	. "$2"
fi

# construct url of installer image and server.iso
bootimg_loc=${archive_mirror}/${distro}/dists/${suite}/main/installer-${arch}/current/images
if [ "$distro" = "debian" ]; then

    if [ "$suite" = "testing" ]; then
        iso_loc=${cdimage_mirror}/cdimage/weekly-builds/${arch}/iso-cd
        iso=$(curl ${iso_loc}/SHA256SUMS | grep -Eo "debian-testing-amd64-netinst.iso")

    else
        iso_loc=${cdimage_mirror}/debian-cd/current/${arch}/iso-cd
        # e.g.: debian-10.4.0-amd64-netinst.iso
        iso=$(curl ${iso_loc}/SHA256SUMS | grep -Eo "${distro}-[[:digit:].]+-${arch}-netinst[.]iso$")
    fi

else

    # use ubuntu-update if it exists
    bootimg_loc_up=${archive_mirror}/${distro}/dists/${suite}-updates/main/installer-${arch}/current/images
    if curl --head --fail ${bootimg_loc_up}; then
        bootimg_loc=${bootimg_loc_up}
    fi

    iso_loc=${cdimage_mirror}/releases/${suite}/release
    # e.g.: ubuntu-18.04.4-server-amd64.iso
    iso=$(curl ${iso_loc}/SHA256SUMS | grep -Eo "${distro}-[[:digit:].]+-server-${arch}[.]iso$")

fi

# construct kernel append for d-i parameters passed to the installer
appends=$(echo "
hostname?=${hostname}
url=${url}
hw-detect/load_firmware=${load_firmware}
${more_appends}
" | tr '\n' ' ')

# where to cache download files
cache=cache/${suite}/${arch}
mkdir -p ${cache}

(cd ${cache}

# get and verify the boot image
# (hd-media dir because that is burned into the SHA256SUMS file)
wget -N --directory-prefix hd-media ${bootimg_loc}/hd-media/boot.img.gz
curl -OJL ${bootimg_loc}/SHA256SUMS
# pull the line out for the 1 file and verify it
grep ./hd-media/boot.img.gz SHA256SUMS > boot.img.gz.SHA256SUM
sha256sum --check boot.img.gz.SHA256SUM

# repo? (cd, file, bunch of bytes) of local .deb's
wget -N ${iso_loc}/${iso}
curl -OJ ${iso_loc}/SHA256SUMS
grep ${iso} SHA256SUMS > ${iso}.SHA256SUM
sha256sum --check ${iso}.SHA256SUM
)

# burn image to media
zcat ${cache}/hd-media/boot.img.gz|sudo dcfldd of=${dev}

# deal with pesky ubuntu builds that don't play well
case $suite in
    trusty|xenial|bionic|disco)
        # bug: "large enough to put an Ubuntu iso in"
        # https://bugs.launchpad.net/ubuntu/+source/debian-installer/+bug/1807047
        # boot.img only has 782M of free space.
        # The .iso is too big and sometimes boot is an iso9660 fs so RO?!!
        fatresize -p -v -s 1G ${dev}
        ;;
esac

# mount the installer media
pmount ${dev}

# install a simple boot cfg using construsted $appends
sed "\|^APPEND|s|$| fb=false ${appends}|" syslinux.cfg | tee /media/${label}/syslinux.cfg

cp ${cache}/${iso} ${cache}/${iso}.SHA256SUM /media/${label}
# check the iso file, make sure it copied ok.
(cd /media/${label}
sha256sum --check ${iso}.SHA256SUM
)


# fix boot.img (now on the usb dev) to be uefi bootable:
# workaround https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=913523
# work towards upstream fix:
# https://salsa.debian.org/carlfk-guest/debian-installer/commits/hd-efi

# don't try if the files don't exist
if [ -f /usr/lib/SYSLINUX.EFI/efi64/syslinux.efi ]; then
    target=/media/$label
    mkdir -p $target/EFI/BOOT $target/EFI/syslinux
    cp /usr/lib/SYSLINUX.EFI/efi64/syslinux.efi $target/EFI/BOOT/BOOTX64.EFI
    cp /usr/lib/syslinux/modules/efi64/ldlinux.e64 $target/EFI/BOOT/
    cp /usr/lib/syslinux/modules/efi64/*.c32 $target/EFI/syslinux
    # end workaround bug=913523
else
    echo "WARNING: NO UEFI SUPPORT: apt install syslinux-efi and try again"
fi

pumount ${dev}
sync

echo Please boot the target machine from the USB stick now.
